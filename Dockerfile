FROM node

WORKDIR /app

RUN apt -y update
RUN apt -y install git
RUN git clone https://gitlab.com/galibert.t/cheh.git
RUN cp -R cheh/* .
RUN npm install 
RUN npm install react-scripts -g
RUN npm install serve -g
RUN npm run build

EXPOSE 3000

CMD ["serve","-s", "dist", "-l", "3000"]
